<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.css">
	<title>Editar producto</title>
</head>
<body>
	<?php 
		$servidor = "localhost";
		$username = "root";
		$pass = "";
		$bd = "tiendaonline";

		$conexion = new mysqli($servidor, $username, $pass, $bd);

		//Revisar si la conexion fue exitosa
		if($conexion->connect_error){
			die("Fallo la conexion al servidor, error: ". $conexion->connect_error);
		}

		$id = $_GET["id"];

		$consulta = "SELECT * FROM productos WHERE id=$id";

		$result = $conexion->query($consulta);

		$producto = mysqli_fetch_assoc($result);
	?>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 offset-sm-3">
				<h2>Editar producto</h2><hr>
				<form action="modificarProducto.php" method="POST">
					<input type="hidden" name="id" value="<?php echo $producto['id'] ?>">
					<div class="form-group">
						<label for="">Nombre:</label>
						<input type="text" name="nombre" placeholder="Teclea el nombre del prodcuto" class="form-control" value="<?php echo $producto['nombre'] ?>" required>
					</div>

					<div class="form-group">
						<label for="">Descripción:</label>
						<textarea name="descripcion" cols="30" rows="5" class="form-control" required><?php echo $producto['descripcion'] ?></textarea>
					</div>

					<div class="form-group">
						<label for="">Precio:</label>
						<input type="number" name="precio" placeholder="Teclea el precio" class="form-control" required value="<?php echo $producto['precio'] ?>">
					</div>

					<div class="from-group">
						<label for="">Cantidad</label>
						<input type="number" name="cantidad" placeholder="Teclea la existencia" class="form-control" required value="<?php echo $producto['cantidad'] ?>">
					</div>

					<div class="form-group">
						<label for="">Categoria</label>
						<input type="text" name="categoria" placeholder="Teclea la categoria" class="form-control" required value="<?php echo $producto['categoria'] ?>">
					</div>

					<div class="form-group">
						<label for="">Marca:</label>
						<input type="text" name="marca" placeholder="Teclea la marca de producto" class="form-control" required value="<?php echo $producto['marca'] ?>">
					</div>
					<input type="submit" value="Guardar" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div>
</body>
</html>