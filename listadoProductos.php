<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.css">
	<title>Listado de productos</title>
</head>
<body>
	<?php include 'obtenerProductos.php'; ?>
	<div class="container">
		<h1>Listado de productos</h1>
		<div class="row">
			<div class="col-12">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Precio</th>
							<th>Cantidad</th>
							<th>Categoría</th>
							<th>Marca</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if($productos->num_rows >0){
								while ($row = $productos->fetch_assoc()) {
									echo "<tr>";
										echo "<td>".$row["id"]."</td>";
										echo "<td>".$row["nombre"]."</td>";
										echo "<td>".$row["descripcion"]."</td>";
										echo "<td>".$row["precio"]."</td>";
										echo "<td>".$row["cantidad"]."</td>";
										echo "<td>".$row["categoria"]."</td>";
										echo "<td>".$row["marca"]."</td>";
										echo "<td>";
										echo "<a href='http://localhost/parcial_3/eliminarProducto.php?id=".$row["id"]."' class='btn btn-sm btn-danger'>Eliminar</a> ";

										echo "<a href='http://localhost/parcial_3/editarProducto.php?id=".$row["id"]."' class='btn btn-sm btn-info'>Editar</a>";
										echo "</td>";
									echo "</tr>";
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>